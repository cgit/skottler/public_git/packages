%global _hardened_build 1
%global previous_name nagios-plugins

Name: monitoring-plugins
Version: 1.5
Release: 4%{?dist}
Summary: Host/service/network monitoring program plugins for a variety of different systems

Group: Applications/System
License: GPLv2+
URL: https://www.%{name}.org/
Source0: https://www.%{name}.org/download/%{previous_name}-%{version}.tar.gz
Source1: %{name}.README.Fedora
Patch2: %{name}-0002-Remove-assignment-of-not-parsed-to-jitter.patch
Patch4: %{name}-0004-Fedora-specific-patch-for-not-to-fixing-fully-qualif.patch
# https://bugzilla.redhat.com/512559
Patch5: %{name}-0005-Prevent-check_swap-from-returning-OK-if-no-swap-acti.patch
Patch7: %{name}-0007-Fix-the-use-lib-statement-and-the-external-ntp-comma.patch
# https://bugzilla.redhat.com/913085
Patch10: %{name}-0010-fix-smart-attribute-comparison.patch

BuildRequires: autoconf automake
BuildRequires: openldap-devel
BuildRequires: mysql-devel
BuildRequires: net-snmp-devel
BuildRequires: net-snmp-utils
BuildRequires: samba-client
BuildRequires: postgresql-devel
BuildRequires: gettext
BuildRequires: %{_bindir}/ssh
BuildRequires: bind-utils
BuildRequires: ntp
BuildRequires: %{_bindir}/mailq
BuildRequires: %{_sbindir}/fping
BuildRequires: perl(Net::SNMP)
BuildRequires: radiusclient-ng-devel
BuildRequires: qstat
BuildRequires: libdbi-devel

Requires: nagios-common >= 3.3.1-1

# monitoring-plugins 1.4.16: the included gnulib files were last updated
# in June/July 2010
# Bundled gnulib exception (https://fedorahosted.org/fpc/ticket/174)
Provides: bundled(gnulib)

%global reqfilt sh -c "%{__perl_requires} | sed -e 's!perl(utils)!monitoring-plugins-perl!'"
%global __perl_requires %{reqfilt}


%description
A bundle of more than fifty standard plugins for Icinga,
Naemon, Nagios, Shinken, Sensu, and other monitoring applications.
Each plugin is a stand-alone command line tool that provides a
specific type of check. Typically, your monitoring software runs
these plugins to determine the current status of hosts and services
on your network.

%package all
Summary: Monitoring Plugins - All plugins
Group: Applications/System
Requires: %{name}-breeze
Requires: %{name}-by_ssh
Requires: %{name}-dhcp
Requires: %{name}-dig
Requires: %{name}-cluster
Requires: %{name}-disk
Requires: %{name}-disk_smb
Requires: %{name}-dns
Requires: %{name}-dummy
Requires: %{name}-file_age
Requires: %{name}-flexlm
Requires: %{name}-fping
Requires: %{name}-game
Requires: %{name}-hpjd
Requires: %{name}-http
Requires: %{name}-icmp
Requires: %{name}-ide_smart
Requires: %{name}-ircd
Requires: %{name}-ldap
Requires: %{name}-load
Requires: %{name}-log
Requires: %{name}-mailq
Requires: %{name}-mrtg
Requires: %{name}-mrtgtraf
Requires: %{name}-mysql
Requires: %{name}-nagios
Requires: %{name}-nt
Requires: %{name}-ntp
Requires: %{name}-ntp-perl
Requires: %{name}-nwstat
Requires: %{name}-oracle
Requires: %{name}-overcr
Requires: %{name}-pgsql
Requires: %{name}-ping
Requires: %{name}-procs
Requires: %{name}-real
Requires: %{name}-rpc
Requires: %{name}-smtp
Requires: %{name}-snmp
Requires: %{name}-ssh
Requires: %{name}-swap
Requires: %{name}-tcp
Requires: %{name}-time
Requires: %{name}-ups
Requires: %{name}-users
Requires: %{name}-wave
%ifnarch ppc ppc64 ppc64p7 sparc sparc64
Requires: monitoring-plugins-sensors
%endif

%description all
This package provides all monitoring plugins.

%package apt
Summary: Monitoring Plugin - check_apt
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description apt
Provides check_apt support.

%package breeze
Summary: Monitoring Plugin - check_breeze
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description breeze
Provides check_breeze support.

%package by_ssh
Summary: Monitoring Plugin - check_by_ssh
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_bindir}/ssh

%description by_ssh
Provides check_by_ssh support.

%package cluster
Summary: Monitoring Plugin - check_cluster
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description cluster
Provides check_cluster support.

%package dbi
Summary: Monitoring Plugin - check_dbi
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description dbi
Provides check_dbi support.

%package dhcp
Summary: Monitoring Plugin - check_dhcp
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: group(nagios)
Requires(pre): group(nagios)

%description dhcp
Provides check_dhcp support.

%package dig
Summary: Monitoring Plugin - check_dig
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_bindir}/dig

%description dig
Provides check_dig support.

%package disk
Summary: Monitoring Plugin - check_disk
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description disk
Provides check_disk support.

%package disk_smb
Summary: Monitoring Plugin - check_disk_smb
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_bindir}/smbclient

%description disk_smb
Provides check_disk_smb support.

%package dns
Summary: Monitoring Plugin - check_dns
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_bindir}/nslookup

%description dns
Provides check_dns support.

%package dummy
Summary: Monitoring Plugin - check_dummy
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description dummy
Provides check_dummy support.
This plugin does not actually check anything, simply provide it with a flag
0-4 and it will return the corresponding status code to Nagios.

%package file_age
Summary: Monitoring Plugin - check_file_age
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description file_age
Provides check_file_age support.

%package flexlm
Summary: Monitoring Plugin - check_flexlm
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description flexlm
Provides check_flexlm support.

%package fping
Summary: Monitoring Plugin - check_fping
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_sbindir}/fping
Requires: group(nagios)
Requires(pre): group(nagios)

%description fping
Provides check_fping support.

%package game
Summary: Monitoring Plugin - check_game
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: qstat

%description game
Provides check_game support.

%package hpjd
Summary: Monitoring Plugin - check_hpjd
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description hpjd
Provides check_hpjd support.

%package http
Summary: Monitoring Plugin - check_http
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description http
Provides check_http support.

%package icmp
Summary: Monitoring Plugin - check_icmp
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: group(nagios)
Requires(pre): group(nagios)

%description icmp
Provides check_icmp support.

%package ide_smart
Summary: Monitoring Plugin - check_ide_smart
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: group(nagios)
Requires(pre): group(nagios)

%description ide_smart
Provides check_ide_smart support.

%package ifoperstatus
Summary: Monitoring Plugin - check_ifoperstatus
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description ifoperstatus
Provides check_ifoperstatus support to monitor network interfaces.

%package ifstatus
Summary: Monitoring Plugin - check_ifstatus
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description ifstatus
Provides check_ifstatus support to monitor network interfaces.

%package ircd
Summary: Monitoring Plugin - check_ircd
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description ircd
Provides check_ircd support.

%package ldap
Summary: Monitoring Plugin - check_ldap
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description ldap
Provides check_ldap support.

%package load
Summary: Monitoring Plugin - check_load
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description load
Provides check_load support.

%package log
Summary: Monitoring Plugin - check_log
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_bindir}/egrep
Requires: %{_bindir}/mktemp

%description log
Provides check_log support.

%package mailq
Summary: Monitoring Plugin - check_mailq
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_bindir}/mailq

%description mailq
Provides check_mailq support.

%package mrtg
Summary: Monitoring Plugin - check_mrtg
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description mrtg
Provides check_mrtg support.

%package mrtgtraf
Summary: Monitoring Plugin - check_mrtgtraf
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description mrtgtraf
Provides check_mrtgtraf support.

%package mysql
Summary: Monitoring Plugin - check_mysql
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description mysql
Provides check_mysql and check_mysql_query support.

%package nagios
Summary: Monitoring Plugin - check_nagios
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description nagios
Provides check_nagios support.

%package nt
Summary: Monitoring Plugin - check_nt
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description nt
Provides check_nt support.

%package ntp
Summary: Monitoring Plugin - check_ntp
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description ntp
Provides check_ntp support.

%package ntp-perl
Summary: Monitoring Plugin - check_ntp.pl
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_sbindir}/ntpdate
Requires: %{_sbindir}/ntpq

%description ntp-perl
Provides check_ntp.pl support.

%package nwstat
Summary: Monitoring Plugin - check_nwstat
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description nwstat
Provides check_nwstat support.

%package oracle
Summary: Monitoring Plugin - check_oracle
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description oracle
Provides check_oracle support.

%package overcr
Summary: Monitoring Plugin - check_overcr
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description overcr
Provides check_overcr support.

%package perl
Summary: Nagios plugins perl dep.
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description perl
Perl dep for nagios plugins.  This is *NOT* an actual plugin it simply provides
utils.pm

%package pgsql
Summary: Monitoring Plugin - check_pgsql
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description pgsql
Provides check_pgsql (PostgreSQL)  support.

%package ping
Summary: Monitoring Plugin - check_ping
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: /bin/ping
Requires: /bin/ping6

%description ping
Provides check_ping support.

%package procs
Summary: Monitoring Plugin - check_procs
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description procs
Provides check_procs support.

%package radius
Summary: Monitoring Plugin - check_radius
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description radius
Provides check_radius support.

%package real
Summary: Monitoring Plugin - check_real
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description real
Provides check_real (rtsp) support.

%package rpc
Summary: Monitoring Plugin - check_rpc
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_sbindir}/rpcinfo

%description rpc
Provides check_rpc support.

%ifnarch ppc ppc64 sparc sparc64
%package sensors
Summary: Monitoring Plugin - check_sensors
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: /bin/egrep
Requires: %{_bindir}/sensors

%description sensors
Provides check_sensors support.
%endif

%package smtp
Summary: Monitoring Plugin - check_smtp
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description smtp
Provides check_smtp support.

%package snmp
Summary: Monitoring Plugin - check_snmp
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Requires: %{_bindir}/snmpgetnext
Requires: %{_bindir}/snmpget

%description snmp
Provides check_snmp support.

%package ssh
Summary: Monitoring Plugin - check_ssh
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description ssh
Provides check_ssh support.

%package swap
Summary: Monitoring Plugin - check_swap
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description swap
Provides check_swap support.

%package tcp
Summary: Monitoring Plugin - check_tcp
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}
Provides: monitoring-plugins-ftp = %{version}-%{release}
Provides: monitoring-plugins-imap = %{version}-%{release}
Provides: monitoring-plugins-jabber = %{version}-%{release}
Provides: monitoring-plugins-nntp = %{version}-%{release}
Provides: monitoring-plugins-ntps = %{version}-%{release}
Provides: monitoring-plugins-pop = %{version}-%{release}
Provides: monitoring-plugins-imap = %{version}-%{release}
Provides: monitoring-plugins-spop = %{version}-%{release}
Provides: monitoring-plugins-ssmtp = %{version}-%{release}
Provides: monitoring-plugins-udp = %{version}-%{release}
Provides: monitoring-plugins-udp2 = %{version}-%{release}
Obsoletes: monitoring-plugins-udp < 1.4.15-2

%description tcp
Provides check_tcp, check_ftp, check_imap, check_jabber, check_nntp,
check_nntps, check_pop, check_simap, check_spop, check_ssmtp, check_udp
and check_clamd support.

%package time
Summary: Monitoring Plugin - check_time
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description time
Provides check_time support.

%package ups
Summary: Monitoring Plugin - check_ups
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description ups
Provides check_ups support.

%package users
Summary: Monitoring Plugin - check_users
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description users
Provides check_users support.

%package wave
Summary: Monitoring Plugin - check_wave
Group: Applications/System
Requires: monitoring-plugins = %{version}-%{release}

%description wave
Provides check_wave support.

%prep
%setup -qn %{previous_name}-%{version}

%patch2 -p1 -b .not_parsed
%patch4 -p1 -b .no_need_fo_fix_paths
%patch5 -p1 -b .fix_missing_swap
%patch7 -p1 -b .ext_ntp_cmds
%patch10 -p1 -b .ssd_smart_params

%build
%configure \
	--libexecdir=%{_libdir}/monitoring/plugins \
	--with-dbi \
	--with-mysql \
	PATH_TO_QSTAT=%{_bindir}/quakestat \
	PATH_TO_FPING=%{_sbindir}/fping \
	PATH_TO_NTPQ=%{_sbindir}/ntpq \
	PATH_TO_NTPDC=%{_sbindir}/ntpdc \
	PATH_TO_NTPDATE=%{_sbindir}/ntpdate \
	PATH_TO_RPCINFO=%{_sbindir}/rpcinfo \
	--with-ps-command="`which ps` -eo 's uid pid ppid vsz rss pcpu etime comm args'" \
	--with-ps-format='%s %d %d %d %d %d %f %s %s %n' \
	--with-ps-cols=10 \
	--enable-extra-opts \
	--with-ps-varlist='procstat,&procuid,&procpid,&procppid,&procvsz,&procrss,&procpcpu,procetime,procprog,&pos'

make %{?_smp_mflags}
cd plugins
make check_ide_smart
make check_ldap
make check_radius
make check_pgsql

cd ..

mv plugins-scripts/check_ntp.pl plugins-scripts/check_ntp.pl.in
gawk -f plugins-scripts/subst plugins-scripts/check_ntp.pl.in > plugins-scripts/check_ntp.pl

cp %{SOURCE1} ./README.Fedora

%install
sed -i 's,^MKINSTALLDIRS.*,MKINSTALLDIRS = ../mkinstalldirs,' po/Makefile
make AM_INSTALL_PROGRAM_FLAGS="" DESTDIR=%{buildroot} install
install -m 0755 plugins-root/check_icmp %{buildroot}/%{_libdir}/monitoring/plugins
install -m 0755 plugins-root/check_dhcp %{buildroot}/%{_libdir}/monitoring/plugins
install -m 0755 plugins/check_ide_smart %{buildroot}/%{_libdir}/monitoring/plugins
install -m 0755 plugins/check_ldap %{buildroot}/%{_libdir}/monitoring/plugins
install -m 0755 plugins-scripts/check_ntp.pl %{buildroot}/%{_libdir}/monitoring/plugins
install -m 0755 plugins/check_radius %{buildroot}/%{_libdir}/monitoring/plugins
install -m 0755 plugins/check_pgsql %{buildroot}/%{_libdir}/monitoring/plugins

%ifarch ppc ppc64 ppc64p7 sparc sparc64
rm -f %{buildroot}/%{_libdir}/monitoring/plugins/check_sensors
%endif

chmod 644 %{buildroot}/%{_libdir}/monitoring/plugins/utils.pm

%find_lang %{previous_name}

%files -f %{previous_name}.lang
%doc ACKNOWLEDGEMENTS AUTHORS CODING COPYING FAQ LEGAL NEWS README REQUIREMENTS SUPPORT README.Fedora
%{_libdir}/monitoring/plugins/negate
%{_libdir}/monitoring/plugins/urlize
%{_libdir}/monitoring/plugins/utils.sh

%files
%{_libdir}/monitoring
%{_libdir}/monitoring/plugins

%files all

%files apt
%{_libdir}/monitoring/plugins/check_apt

%files breeze
%{_libdir}/monitoring/plugins/check_breeze

%files by_ssh
%{_libdir}/monitoring/plugins/check_by_ssh

%files cluster
%{_libdir}/monitoring/plugins/check_cluster

%files dbi
%{_libdir}/monitoring/plugins/check_dbi

%files dhcp
%defattr(4750,root,nagios,-)
%{_libdir}/monitoring/plugins/check_dhcp

%files dig
%{_libdir}/monitoring/plugins/check_dig

%files disk
%{_libdir}/monitoring/plugins/check_disk

%files disk_smb
%{_libdir}/monitoring/plugins/check_disk_smb

%files dns
%{_libdir}/monitoring/plugins/check_dns

%files dummy
%{_libdir}/monitoring/plugins/check_dummy

%files file_age
%{_libdir}/monitoring/plugins/check_file_age

%files flexlm
%{_libdir}/monitoring/plugins/check_flexlm

%files fping
%defattr(4750,root,nagios,-)
%{_libdir}/monitoring/plugins/check_fping

%files game
%{_libdir}/monitoring/plugins/check_game

%files hpjd
%{_libdir}/monitoring/plugins/check_hpjd

%files http
%{_libdir}/monitoring/plugins/check_http

%files icmp
%defattr(4750,root,nagios,-)
%{_libdir}/monitoring/plugins/check_icmp

%files ifoperstatus
%{_libdir}/monitoring/plugins/check_ifoperstatus

%files ifstatus
%{_libdir}/monitoring/plugins/check_ifstatus

%files ide_smart
%defattr(4750,root,nagios,-)
%{_libdir}/monitoring/plugins/check_ide_smart

%files ircd
%{_libdir}/monitoring/plugins/check_ircd

%files ldap
%{_libdir}/monitoring/plugins/check_ldap
%{_libdir}/monitoring/plugins/check_ldaps

%files load
%{_libdir}/monitoring/plugins/check_load

%files log
%{_libdir}/monitoring/plugins/check_log

%files mailq
%{_libdir}/monitoring/plugins/check_mailq

%files mrtg
%{_libdir}/monitoring/plugins/check_mrtg

%files mrtgtraf
%{_libdir}/monitoring/plugins/check_mrtgtraf

%files mysql
%{_libdir}/monitoring/plugins/check_mysql
%{_libdir}/monitoring/plugins/check_mysql_query

%files nagios
%{_libdir}/monitoring/plugins/check_nagios

%files nt
%{_libdir}/monitoring/plugins/check_nt

%files ntp
%{_libdir}/monitoring/plugins/check_ntp
%{_libdir}/monitoring/plugins/check_ntp_peer
%{_libdir}/monitoring/plugins/check_ntp_time

%files ntp-perl
%{_libdir}/monitoring/plugins/check_ntp.pl

%files nwstat
%{_libdir}/monitoring/plugins/check_nwstat

%files oracle
%{_libdir}/monitoring/plugins/check_oracle

%files overcr
%{_libdir}/monitoring/plugins/check_overcr

%files perl
%{_libdir}/monitoring/plugins/utils.pm

%files pgsql
%{_libdir}/monitoring/plugins/check_pgsql

%files ping
%{_libdir}/monitoring/plugins/check_ping

%files procs
%{_libdir}/monitoring/plugins/check_procs

%files radius
%{_libdir}/monitoring/plugins/check_radius

%files real
%{_libdir}/monitoring/plugins/check_real

%files rpc
%{_libdir}/monitoring/plugins/check_rpc

%ifnarch ppc ppc64 ppc64p7 sparc sparc64
%files sensors
%{_libdir}/monitoring/plugins/check_sensors
%endif

%files smtp
%{_libdir}/monitoring/plugins/check_smtp

%files snmp
%{_libdir}/monitoring/plugins/check_snmp

%files ssh
%{_libdir}/monitoring/plugins/check_ssh

%files swap
%{_libdir}/monitoring/plugins/check_swap

%files tcp
%{_libdir}/monitoring/plugins/check_clamd
%{_libdir}/monitoring/plugins/check_ftp
%{_libdir}/monitoring/plugins/check_imap
%{_libdir}/monitoring/plugins/check_jabber
%{_libdir}/monitoring/plugins/check_nntp
%{_libdir}/monitoring/plugins/check_nntps
%{_libdir}/monitoring/plugins/check_pop
%{_libdir}/monitoring/plugins/check_simap
%{_libdir}/monitoring/plugins/check_spop
%{_libdir}/monitoring/plugins/check_ssmtp
%{_libdir}/monitoring/plugins/check_tcp
%{_libdir}/monitoring/plugins/check_udp

%files time
%{_libdir}/monitoring/plugins/check_time

%files ups
%{_libdir}/monitoring/plugins/check_ups

%files users
%{_libdir}/monitoring/plugins/check_users

%files wave
%{_libdir}/monitoring/plugins/check_wave

%changelog
* Mon Feb 3 2014 Sam Kottler <skottler@fedoraproject.org> - 1.5-4
- A few changes based on feedback from misc

* Tue Jan 28 2014 Sam Kottler <skottler@fedoraproject.org> - 1.5-3
- Bump the version for the name change

* Thu Oct 24 2013 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.5-2
- New check_dbi plugin (BR: libdbi-devel; subpackage: monitoring-pluginsdbi)

* Wed Oct 23 2013 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.5-1
- Update to version 1.5
- New project homepage and source download locations
- Disabled patches 1, 6, 8, and 9.
- No linux_raid subpackage (the contrib directory was removed)

* Wed Oct 16 2013 Peter Lemenkov <lemenkov@gmail.com> - 1.4.16-10
- Remove EL4 and EL5 support
- Backport patches to fix check_linux_raid in case of resyncing (rhbz #504721)
- Fix smart attribute comparison (rhbz #913085)

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.16-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun Jul 21 2013 Petr Pisar <ppisar@redhat.com> - 1.4.16-8
- Perl 5.18 rebuild

* Wed May 22 2013 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.4.16-7
- Build package with PIE flags (#965536)

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.16-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Aug 17 2012 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.4.16-5
- Fix the use lib statement and the external ntp commands paths in check-ntp.pl
  (monitoring-plugins0008-ntpdate-and-ntpq-paths.patch).

* Thu Aug 16 2012 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.4.16-4
- Remove the erroneous requirements of monitoring-pluginsntp (#848830)
- Ship check-ntp.pl in the new monitoring-pluginsntp-perl subpackage (#848830)

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.16-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Jul  9 2012 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.4.16-2
- Provides bundled(gnulib) (#821779)

* Mon Jul  9 2012 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.4.16-1
- Update to version 1.4.16
- Dropped monitoring-plugins0005-Patch-for-check_linux_raid-with-on-linear-raid0-arra.patch
  (upstream).

* Tue Jun 26 2012 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.4.15-7
- glibc 2.16 no longer defines gets for ISO C11, ISO C++11, and _GNU_SOURCE
  (#835621): monitoring-plugins0007-undef-gets-and-glibc-2.16.patch

* Tue Jun 26 2012 Jose Pedro Oliveira <jpo at di.uminho.pt> - 1.4.15-6
- The nagios-plugins RPM no longer needs to own the /usr/lib{,64}/nagios/plugins
  directory; this directory is now owned by nagios-common (#835621)
- Small updates (clarification) to the file nagios-plugins.README.Fedora

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.15-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Mar 23 2011 Dan Horák <dan@danny.cz> - 1.4.15-4
- rebuilt for mysql 5.5.10 (soname bump in libmysqlclient)

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.15-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Oct  7 2010 Peter Lemenkov <lemenkov@gmail.com> - 1.4.15-2
- Dropped check_udp sub-package (see rhbz #634067). Anyway it
  provided just a symlink to check_tcp.
- Fixed weird issue with check_swap returning ok in case of
  missing swap (see rhbz #512559).

* Wed Aug 18 2010 Peter Lemenkov <lemenkov@gmail.com> - 1.4.15-1
- Ver. 1.4.15
- Dropped patch for restoration of behaviour in case of ssl checks

* Tue May 18 2010 Peter Lemenkov <lemenkov@gmail.com> - 1.4.14-4
- Restore ssl behaviour for check_http in case of self-signed
  certificates (see rhbz #584227).

* Sat Apr 24 2010 Peter Lemenkov <lemenkov@gmail.com> - 1.4.14-3
- Removed Requires - nagios (see rhbz #469530).
- Added "Requires,Requires(pre): group(nagios)" where necessary
- Sorted %%files sections
- No need to ship INSTALL file
- Added more doc files to main package

* Mon Apr 12 2010 Peter Lemenkov <lemenkov@gmail.com> - 1.4.14-2
- Added missing Requires - nagios (see rhbz #469530).
- Fixed path to qstat -> quakestat (see rhbz #533777)
- Disable radius plugin for EL4 - there is not radiuscleint-ng for EL-4

* Wed Mar 10 2010 Peter Lemenkov <lemenkov@gmail.com> - 1.4.14-1
- Ver. 1.4.14
- Rebased patches.

* Fri Aug 21 2009 Tomas Mraz <tmraz@redhat.com> - 1.4.13-17
- rebuilt with new openssl

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.13-16
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jun 22 2009 Mike McGrath <mmcgrath@redhat.com> - 1.4.13-15
- Added patch from upstream to fix ntp faults (bz #479030)

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.13-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 24 2009 Caolán McNamara <caolanm@redhat.com> 1.4.13-13
- rebuild for dependencies

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> 1.4.13-12
- rebuild with new openssl

* Mon Oct 20 2008 Robert M. Albrecht <romal@gmx.de> 1.4.13-11
- Enabled --with-extra-opts again

* Mon Oct 20 2008 Robert M. Albrecht <romal@gmx.de> 1.4.13-10
- removed provides perl plugins Bugzilla 457404

* Thu Oct 16 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.13-9
- This is a "CVS is horrible" rebuild

* Thu Oct  9 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.13-8
- Rebuilt with a proper patch

* Wed Oct  8 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.13-7
- Added changed recent permission changes to allow nagios group to execute

* Wed Oct  8 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.13-6
- Fixed up some permission issues

* Mon Oct  6 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.13-5
- Fixing patch, missing semicolon

* Sun Sep 28 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.13-4
- Upstream released new version #464419
- Added patch fix for check_linux_raid #253898
- Upstream releases fix for #451015 - check_ntp_peers
- Upstream released fix for #459309 - check_ntp
- Added Provides Nagios::Plugins for #457404
- Fixed configure line for #458985 check_procs

* Thu Jul 10 2008 Robert M. Albrecht <romal@gmx.de> 1.4.12-3
- Removed --with-extra-opts, does not build in Koji

* Mon Jun 30 2008 Robert M. Albrecht <romal@gmx.de> 1.4.12-2
- Enabled --with-extra-opts

* Sun Jun 29 2008 Robert M. Albrecht <romal@gmx.de> 1.4.12-1
- Upstream released version 1.4.12
- Removed patches ping_timeout.patch and pgsql-fix.patch

* Wed Apr 30 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.11-4
- added patch for check_pgsql

* Wed Apr 09 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.11-2
- Fix for 250588

* Thu Feb 28 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.11-1
- Upstream released version 1.4.11
- Added check_ntp peer and time

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.4.10-6
- Autorebuild for GCC 4.3

* Tue Feb 12 2008 Mike McGrath <mmcgrath@redhat.com> 1.4-10-5
- Rebuild for gcc43

* Thu Jan 10 2008 Mike McGrath <mmcgrath@redhat.com> 1.4.10-4
- Fixed check_log plugin #395601

* Thu Dec 06 2007 Release Engineering <rel-eng at fedoraproject dot org> - 1.4.10-2
- Rebuild for deps

* Thu Dec 06 2007 Mike McGrath <mmcgrath@redhat.com> 1.4.10-1
- Upstream released new version
- Removed some patches

* Fri Oct 26 2007 Mike McGrath <mmcgrath@redhat.com> 1.4.8-9
- Fix for Bug 348731 and CVE-2007-5623

* Wed Aug 22 2007 Mike McGrath <mmcgrath@redhat.com> 1.4.8-7
- Rebuild for BuildID
- License change

* Fri Aug 10 2007 Mike McGrath <mmcgrath@redhat.com> 1.4.8-6
- Fix for check_linux_raid - #234416
- Fix for check_ide_disk - #251635

* Tue Aug 07 2007 Mike McGrath <mmcgrath@redhat.com> 1.4.8-2
- Fix for check_smtp - #251049

* Fri Apr 13 2007 Mike McGrath <mmcgrath@redhat.com> 1.4.8-1
- Upstream released new version

* Fri Feb 23 2007 Mike McGrath <mmcgrath@redhat.com> 1.4.6-1
- Upstream released new version

* Sun Dec 17 2006 Mike McGrath <imlinux@gmail.com> 1.4.5-1
- Upstream released new version

* Fri Oct 27 2006 Mike McGrath <imlinux@gmail.com> 1.4.4-2
- Enabled check_smart_ide
- Added patch for linux_raid
- Fixed permissions on check_icmp

* Tue Oct 24 2006 Mike McGrath <imlinux@gmail.com> 1.4.4-1
- Upstream new version
- Disabled check_ide_smart (does not compile cleanly/too lazy to fix right now)
- Added check_apt

* Sun Aug 27 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-18
- Removed utils.pm from the base nagios-plugins package into its own package

* Tue Aug 15 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-17
- Added requires qstat for check_game

* Thu Aug 03 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-16
- Providing path to qstat

* Thu Aug 03 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-15
- Fixed permissions on check_dhcp
- Added check_game
- Added check_radius
- Added patch for ntp

* Sun Jul 23 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-14
- Patched upstream issue: 196356

* Sun Jul 23 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-13
- monitoring-pluginsall now includes nagios-plugins-mysql

* Thu Jun 22 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-12
- removed sensors support for sparc and sparc64

* Thu Jun 22 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-11
- Created a README.Fedora explaining how to install other plugins

* Sun Jun 11 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-9
- Removed check_sensors in install section

* Sat Jun 10 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-8
- Inserted conditional blocks for ppc exception.

* Wed Jun 07 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-7
- Removed sensors from all plugins and added excludearch: ppc

* Tue Jun 06 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-6
- For ntp plugins requires s/ntpc/ntpdc/

* Sat Jun 03 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-5
- Fixed a few syntax errors and removed an empty export

* Fri May 19 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-4
- Now using configure macro instead of ./configure
- Added BuildRequest: perl(Net::SNMP)
- For reference, this was bugzilla.redhat.com ticket# 176374

* Fri May 19 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-3
- Added check_ide_smart
- Added some dependencies
- Added support for check_if* (perl-Net-SNMP now in extras)
- nagios-plugins now owns dir %%{_libdir}/monitoring

* Sat May 13 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-2
- Added a number of requires that don't get auto-detected

* Sun May 07 2006 Mike McGrath <imlinux@gmail.com> 1.4.3-1
- Upstream remeased 1.4.3

* Tue Apr 18 2006 Mike McGrath <imlinux@gmail.com> 1.4.2-9
- Fixed a typo where monitoring-pluginsall required nagios-plugins-httpd

* Mon Mar 27 2006 Mike McGrath <imlinux@gmail.com> 1.4.2-8
- Updated to CVS head for better MySQL support

* Sun Mar 5 2006 Mike McGrath <imlinux@gmail.com> 1.4.2-7
- Added a monitoring-pluginsall package

* Wed Feb 1 2006 Mike McGrath <imlinux@gmail.com> 1.4.2-6
- Added provides for check_tcp

* Mon Jan 30 2006 Mike McGrath <imlinux@gmail.com> 1.4.2-5
- Created individual packages for all check_* scripts

* Tue Dec 20 2005 Mike McGrath <imlinux@gmail.com> 1.4.2-4
- Fedora friendly spec file

* Mon May 23 2005 Sean Finney <seanius@seanius.net> - cvs head
- just include the nagios plugins directory, which will automatically include
  all generated plugins (which keeps the build from failing on systems that
  don't have all build-dependencies for every plugin)

* Thu Mar 04 2004 Karl DeBisschop <karl[AT]debisschop.net> - 1.4.0alpha1
- extensive rewrite to facilitate processing into various distro-compatible specs

* Thu Mar 04 2004 Karl DeBisschop <karl[AT]debisschop.net> - 1.4.0alpha1
- extensive rewrite to facilitate processing into various distro-compatible specs


Name:       libsodium
Version:	0.4.2
Release:	1%{?dist}
Summary:	P(ortable|ackageable) NaCl-based crypto library

Group:		System Environemnt/Libraries
License:	GPLv3
URL:	    https://github.com/jedisct1/%{name}
Source0:	https://github.com/jedisct1/%{name}/releases/download/%{version}/%{name}-%{version}.tar.gz
BuildRoot:  %{_tmppath}/%{name}-%{version}-root

#BuildRequires:	
#Requires:	

%description
NaCl (pronounced "salt") is a new easy-to-use high-speed software library for network communication, encryption, decryption, signatures, etc.

NaCl's goal is to provide all of the core operations needed to build higher-level cryptographic tools.

%package devel
Summary: Files for development of application which will use libsodium
Group: Development/Libraries

%description devel
NaCl (pronounced "salt") is a new easy-to-use high-speed software library for
network communication, encryption, decryption, signatures, etc. The
libsodium-devel package contains various include files needed to develop
applications which rely on libsodium.

%prep
%setup -q -n %{name}-%{version}


%build
%configure
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
%defattr(-, root,root)
%doc

%files devel
%defattr(-,root,root)
%{_includedir}/sodium.h
%{_includedir}/sodium
%{_libdir}/*.so
%{_prefix}/%{_lib}/libsodium.a
%{_prefix}/%{_lib}/libsodium.so.*
%{_prefix}/%{_lib}/libsodium.la

# %files static
#%defattr(-,root,root)
#%attr(0644,root,root) %{_libdir}/*.a

%changelog

